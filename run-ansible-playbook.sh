#!/usr/bin/env bash
#
# Wrapper script which runs correct version of Ansible for this repository.
# Forwards the user's ssh agent into the container so that Ansible can log into
# the machines it needs to.
#
# The script will fail to run if it cannot decrypt the vault password or if the
# user has not got an SSH agent running.

# Exit on error
set -e

# Get the directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Use the open vault script to get the repository password.
export VAULT_PASSWORD=$(${DIR}/secrets/open-vault)

# Check that the vault passowrd was decrypted.
if [ -z "${VAULT_PASSWORD}" ]; then
  echo "$0: could not decrypt vault password" >&2
  exit 1
fi

# Check that a SSH agent is configured
if [ -z "${SSH_AUTH_SOCK}" ]; then
  echo "$0: ssh agent not running" >&2
  exit 1
fi

case "$OSTYPE" in
    darwin*)
        [ ! "$(docker ps | grep pinata-sshd)" ] && pinata-ssh-forward
        ssh_agent_options="$(pinata-ssh-mount)"
        ;;
    *)
        ssh_agent_options="-v ${SSH_AUTH_SOCK}:/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent"
        ;;
esac

exec docker run \
    -t --rm \
    -e VAULT_PASSWORD -e TERRAFORM_OUTPUTS \
    ${ssh_agent_options} \
    -v $PWD:/workspace:ro \
    uisautomation/ansible-playbook:2.7 \
    "$@"
